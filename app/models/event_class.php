<?php
    class Eventclass extends AppModel {
    	var $name = 'EventClass';
		var $belongsTo = array('Event');
        var $validate = array(
            	'index' => array(
                	'rule' => 'notEmpty'
                ),  
            	'name' => array(
                	'rule' => 'notEmpty'
                )
		);
    }
?>