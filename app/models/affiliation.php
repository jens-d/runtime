<?php
    class Affiliation extends AppModel {
    	var $name = 'Affiliation';
		var $hasMany = array('Runner');
        var $validate = array(
				'name' => array(
				'rule' => 'notEmpty'
			)
		);
    }
?>