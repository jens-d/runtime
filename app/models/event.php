<?php
    class Event extends AppModel {
    	var $name = 'Event';
		var $hasMany = array('EventClass', 'Runner');
        var $validate = array(
				'name' => array(
				'rule' => 'notEmpty'
			)
		);
		
		public function getCurrentEvent() {
			$this->id = Configure::read('Event.id');
			return $this->read();
		}
    }
?>