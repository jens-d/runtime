<?php
    class Runner extends AppModel {
    	var $name = 'Runner';
		var $belongsTo = array('Event', 'EventClass', 'Affiliation');
        var $validate = array(
            	'start_number' => array(
                	'rule' => 'notEmpty'
                ),  
            	'first_name' => array(
                	'rule' => 'notEmpty'
                ),
            	'last_name' => array(             
                	'rule' => 'notEmpty'
                )
		);
    }
?>