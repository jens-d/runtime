; This file, 'lynx.sch', contains the schedule of events.

; Format:

; event number, round number, heat number
; event number, round number, heat number
; .
; .
; .

; Notes:

; + Any line starting with a semicolon is ignored.
; + If you omit the heat number or the round and heat numbers they will
;   default to 1.

41,6,1
41,6,2
41,6,3
41,6,4
41,6,5
29,3,1
9,3,1
23,2,1
3,2,1
21,3,1
1,3,1
25,2,1
41,0,1
5,2,1
22,3,1
2,3,1
26,2,1
7,1,1
32,2,1
12,2,1
