<?php
    class PostsController extends AppController {
    	var $name = 'Posts';
		var $helpers = array('Html','Javascript');

        function index() {
                $this->set('posts', $this->Post->find('all'));
        }

        function add() {
            if (!empty($this->data)) {
                if ($this->Post->save($this->data)) {
                    $this->Session->setFlash('Your post has been saved.');
                    $this->redirect(array('action' => 'index'));
                }
            }
            $this->set('posts', $this->Post->find('all'));
        }
    }
?>