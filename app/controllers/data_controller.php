<?php
    Configure::load('event');

    class DataController extends AppController {
    	var $name = 'Data';
		var $uses = array();
		var $helpers = array('Html','Javascript');
			
		static private $_import_path = '';
		
		function beforeFilter() {
			parent::beforeFilter();
			self::$_import_path = WWW_ROOT . 'files' . DS;
		}

		/*
		 * Custom methods
		 */
		
		static private function _getFinishLynxDataFormat($format_type = 'event') {
			
			$data_format = array();
			$data_format['event'] = array(
				'event_number', 
				'round_number', 
				'heat_number', 
				'event_name',
				'wind',
				'wind_unit',
				'template',
				'capture_time',
				'capture_duration',
				'distance',
				'time_trial_start_time'
			);
			$data_format['runner'] = array(
				'place', 
				'id', 
				'lane', 
				'last_name', 
				'first_name',
				'affiliation',
				'time',
				'license',
				'delta_time',
				'ReacTime',
				'splits',
				'time_trial_start_time',
				'user_1',
				'user_2',
				'user_3',
				'user_4',
				'user_5'
			);
			
			if (array_key_exists($format_type, $data_format)) {
				return $data_format[$format_type];
			} else {
				return array();
			}
		}
		
		private function _importRunnerResultData($data, $replace_data = false) {
			
			$importResult = array();
			$importResult['runnersFound'] = 0;
			$importResult['runnersFailed'] = 0;
			if (is_array($data) && !empty($data)) {
				App::import('Model', 'Runner');
				$Runner = new Runner();
				$importResult['saved'] = array();
				$importResult['failed'] = array();
				$dataModelFields = array(
					'time' => 'result'
				);
				// Walk through the participant data
				foreach ($data as $runner) {
					$result = $Runner->find('first', array(
							'conditions' => array(
								"Runner.start_number" => (int)$runner['id']
							)
						)
					);
					if ($result !== false) {
						$Runner->id = (int)$result['Runner']['id'];
						if ($Runner->read()) {
							// Walk through the data fields
							foreach ($dataModelFields as $dataField => $modelField) {
								if ($dataField == 'time') {
									$runner[$dataField] = preg_replace("/^(\d+)[,|\.](\d+)$/", '0:$1.$2', $runner[$dataField]);
									$runner[$dataField] = preg_replace("/^(\d+)[,|\.](\d+),(\d+)$/", '$1:$2.$3', $runner[$dataField]);
									$runner[$dataField] = preg_replace("/^(\d{1})\:(\d+)\.(\d+)$/", '0$1:$2.$3', $runner[$dataField]);
									if (empty($runner[$dataField])) {
										$runner[$dataField] = null;
									}
								}
								$Runner->set($modelField, $runner[$dataField]);
							}
							if ($Runner->save()) {
								$importResult['runnersFound']++;
								$importResult['saved'][] = $runner;
							} else {
								$importResult['runnersFailed']++;
								$importResult['failed'][] = $runner;
							}
						}
					}
				}
			} 
			return $importResult;
		}
		
		private function _getFileData($filename) {
			
			$data = array();
			$File = new File(self::$_import_path. $filename, false);
			if ($File->exists() && $File->readable() && $File->size() > 0) {
				$filedata = $File->read(true);
				$lines = explode("\n", $filedata);
				$event = array_combine(
					self::_getFinishLynxDataFormat('event'), 
					explode(",", array_shift($lines))
				);
				
				$data['event'] = $event;
				
				$data['runners'] = array();
				foreach ($lines as $line) {
					$runner = array_combine(
						self::_getFinishLynxDataFormat('runner'), 
						explode(",", $line)
					);
					$data['runners'][] = $runner;
				}
			}
			
			return $data;
		}
		
		private function _saveFileData($filename, $data) {
			
			$filename = 'lynx.ppl';
			$File = new File(self::$_import_path. $filename, $create = true);
			if ($File->writable()) {
				$File->write($File->prepare($data));
			}
		}
		
		
		/*
		 * Views
		 */
		
		function action() {
			$this->layout = '';
			
			header('Content-type: text/plain; charset=UTF-8');
			
			$filename = 'event-1.lif';
			
		}
		
		function export() {
			$this->set('data_types', array(
					1 => 'FinishLynx (PPL-fil)',
					2 => 'Deltagare (CSV-fil)'
				)
			);
		}

		function index() {
		}

		function import() {

			$Folder = new Folder(self::$_import_path);
			list($folders, $filelist) = $Folder->read();
			$files = array(0 => '-');
			foreach ($filelist as $file) {
				if (isset($this->params['url']['data_type']) && $this->params['url']['data_type'] > 0) {
					if ($this->params['url']['data_type'] == 1) {
						if (substr($file, -4) != '.lif') {
							continue;
						}
					} else if ($this->params['url']['data_type'] == 2) {
						if (substr($file, -4) != '.csv') {
							continue;
						}
					}
				}
				$files[$file] = $file;
			}
			
			if (!empty($this->data)) {
				if ($this->data['Import']['local_file'] != '0') {
					$filedata = $this->_getFileData($this->data['Import']['local_file']);
					if (!empty($filedata)) {
						$result = $this->_importRunnerResultData($filedata['runners']);
						$flash = sprintf("Sparade resultat för %d deltagare. ", $result['runnersFound']);
						if ($result['runnersFailed'] > 0) {
							$flash .= sprintf("Misslyckades att spara resultat för %d deltagare. ", $result['runnersFailed']);
							$flash .= print_r($result['failed'], true);
						}
						$this->Session->setFlash($flash);
						$this->redirect(array('action' => 'index'));
					}
				}
			}
			
			$this->set('files', $files);
			$this->set('data_types', array(
					0 => 'Välj ...', 
					1 => 'FinishLynx resultat',
					2 => 'Deltagare (CSV-fil)'
				)
			);
			if (isset($this->params['url']['data_type']) && $this->params['url']['data_type'] > 0) {
				$this->set('data_type', $this->params['url']['data_type']);
			}
		}
		
		function output() {
			$this->layout = '';
			
			$data = "";
			$data .= "; This file 'lynx.ppl' matches ID numbers with names and affiliations". PHP_EOL;
			$data .= "; Format:". PHP_EOL;
			$data .= "; ID number, last name, first name, affiliation". PHP_EOL;
			
			
			App::import('Model', 'Runner');
			$Runner = new Runner();
			$runners = $Runner->find('all', array(
					'fields' => array('Runner.start_number', 'Runner.first_name', 'Runner.last_name', 'Affiliation.name')
				)
			);
			foreach ($runners as &$runner) {
				$data .= sprintf("%d,%s,%s,%s", $runner['Runner']['start_number'], $runner['Runner']['last_name'], $runner['Runner']['first_name'], $runner['Affiliation']['name']);
				$data .= PHP_EOL;
			}
			
			header("Content-type: text/plain; charset=UTF-8");
			echo $data;
			$this->_saveFileData('lynx.ppl', $data);
			exit(0);
		}
    }
?>