<?php
    Configure::load('event');

    class EventsController extends AppController {
    	var $name = 'Events';
		var $helpers = array('Html','Javascript');
		
        function results() {

        }

		function startlist() {
		
			$event = $this->Event->getCurrentEvent();
			$this->layout = 'print';
			$this->pageTitle = $event['Event']['name'].' - Startlista';
			
			App::import('Model', 'Runner');
			$Runner = new Runner();
			$conditions = array("Runner.event_id = ".Configure::read('Event.id'));
			if (isset($this->passedArgs['class']) && is_numeric($this->passedArgs['class'])) {
				array_push($conditions, "Runner.event_class_id =".$this->passedArgs['class']);
			}
			$runners = $Runner->find('all', array(
				'conditions' => $conditions,
				'order' => array("Runner.event_class_id ASC", "Runner.start_number ASC"),
				'fields' => array('Runner.start_number', 'Runner.first_name', 'Runner.last_name', 'Runner.birth_year', 'Affiliation.name', 'EventClass.index','EventClass.name')
				)
			);
			foreach ($runners as &$runner) {
				$runner['Runner']['birth_year'] = $runner['Runner']['birth_year'] == '0000' ? '-' : $runner['Runner']['birth_year'];
				$runner['Affiliation']['name'] = $runner['Affiliation']['name'] == '' ? '-' : $runner['Affiliation']['name'];
			}
			$this->set('runners', $runners);
		}
    }
?>