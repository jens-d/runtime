<?php
    Configure::load('event');

    class ResultsController extends AppController {
    	var $name = 'Results';
		var $uses = array();
		var $helpers = array('Html','Javascript');
			

		function index() {
			App::import('Model', 'EventClass');
			$EventClass = new EventClass();
			$classes = $EventClass->find('all', array(
					'fields' => array('EventClass.id', 'EventClass.index', 'EventClass.name'),
					'conditions' => array('EventClass.event_id' => Configure::read('Event.id'))
				)
			);
			$Runner = new Runner();
			foreach ($classes as &$class) {
			    $class['EventClass']['total_results'] = $Runner->find('count', array(
						'conditions' => array('Runner.event_class_id' => $class['EventClass']['id'], 'Runner.result IS NOT NULL')
					)
			    );
			}
			$this->set('classes', $classes);
		}

		function add() {

			if (!empty($this->data)) {

				App::import('Model', 'Runner');
				$Runner = new Runner();
				if ($this->data['Result']['result_extra'] === '' || !is_numeric($this->data['Result']['result_extra'])) {
					$this->data['Result']['result_extra'] = 0;
				}
				$result = $Runner->find('first', array(
						'conditions' => array(
							"Runner.id !=" => $this->data['Result']['runner_id'],
							"Runner.result" => $this->data['Result']['result'],
							"Runner.event_class_id" => $this->data['Result']['event_class_id']
						)
					)
				);
				$this->data['Result']['result'] = preg_replace("/^(\d+),(\d+)$/", '0:$1.$2', $this->data['Result']['result']);
				$this->data['Result']['result'] = preg_replace("/^(\d+),(\d+),(\d+)$/", '$1:$2.$3', $this->data['Result']['result']);
				// var_dump($this->data['Result']['result']);die;
				$this->Runner = new Runner($this->data['Result']['runner_id']);
				$this->Runner->set('result', $this->data['Result']['result']);
				$this->Runner->set('result_extra', $this->data['Result']['result_extra']);
				if ($this->Runner->save()) {
				}
				if ($result === false) {
					
					$this->Session->setFlash(sprintf('Sparade resultatet "%s" för startnummer %d.', $this->data['Result']['result'], $this->data['Result']['start_number']));
					$this->redirect(array('action' => 'add'));
					
				} else {
					
					$this->set("newResultExtra", $result['Runner']['result_extra']);
					$this->Session->setFlash(sprintf('Resultatet "%s" fanns redan registrerat på deltagaren med startnummer %d i denna klass. Vänligen bekräfta en inbördes placering.', $result['Runner']['result'], $result['Runner']['start_number']));
				}
			}
		}

		function resultlist() {
			
			if ($this->passedArgs['media'] == 'print') {
				$event = ClassRegistry::init('Event')->getCurrentEvent();
				$this->layout = 'print';
				$this->pageTitle = $event['Event']['name'].' - Resultatlista';
			}
			
			App::import('Model', 'Runner');
			$Runner = new Runner();
			$conditions = array("Runner.result IS NOT NULL", "Runner.event_id = ".Configure::read('Event.id'));
			if (isset($this->passedArgs['class']) && is_numeric($this->passedArgs['class'])) {
				array_push($conditions, "Runner.event_class_id =".$this->passedArgs['class']);
			}
			$runners = $Runner->find('all', array(
				'conditions' => $conditions,
				'order' => array("Runner.event_class_id ASC", "(0 + Runner.result)", "Runner.result", "Runner.result_extra"),
				'fields' => array('Runner.result', 'Runner.start_number', 'Runner.first_name', 'Runner.last_name', 'Runner.birth_year', 'Runner.event_class_id', 'Affiliation.name', 'EventClass.index','EventClass.name')
				)
			);
			$event_classes = array();
			foreach ($runners as &$runner) {
				$runner['Runner']['birth_year'] = $runner['Runner']['birth_year'] == '0000' ? '-' : $runner['Runner']['birth_year'];
				$event_classes[$runner['Runner']['event_class_id']][] = $runner;
			}
			$runners = array();
			foreach (array_keys($event_classes) as $event_class) {				
				usort($event_classes[$event_class], array($this, '_cmpRunnerResult'));
				$runners = array_merge($runners, $event_classes[$event_class]);
			}
			$this->set('runners', $runners);
		}
		
		private function _cmpRunnerResult($a, $b) {
			$a['Runner']['result'] = preg_replace("/^(\d+)\:(\d+)\.(\d+)$/", "0:$1:$2.$3", $a['Runner']['result']);
			$b['Runner']['result'] = preg_replace("/^(\d+)\:(\d+)\.(\d+)$/", "0:$1:$2.$3", $b['Runner']['result']);
			
			return strcmp($a['Runner']['result'], $b['Runner']['result']);
		}
    }
?>