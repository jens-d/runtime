<?php
    class AjaxController extends AppController {
    	var $name = 'Ajax';
    	var $uses = array();
        var $helpers = array('Html', 'Javascript');
        var $components = array('RequestHandler');

        function beforeFilter() {
	    parent::beforeFilter();

	    // Must be disabled or AJAX calls fail
	    $this->Security->validatePost = false;

	    $this->layout = 'ajax'; // Or $this->RequestHandler->ajaxLayout, Only use for HTML
	    $this->autoLayout = false;
	    $this->autoRender = false;
        }
		
        function index() {
 
	    $response = array('success' => false);

	    if (!empty($this->data)) {
		    switch ($this->data['action']) {
				case "get_last_runner":
					if (is_numeric($this->data['event_class_id'])) {
						App::import('Model', 'Runner');
						$Runner = new Runner();
						$result = $Runner->find('first', array('conditions' => "Runner.event_class_id = ".$this->data['event_class_id'], 'order' => "Runner.start_number DESC"));
						if ($result) {
							$response['success'] = true;
							$response['runner'] = $result['Runner'];
						}
					}
					break;
				case "autocomplete":
					if ($this->data['key'] == "affiliation") {

						App::import('Model', 'Affiliation');
						$Affiliation = new Affiliation();
						$result = $Affiliation->find('all', array('conditions' => "Affiliation.name LIKE '".$this->data['term']."%'"));
						foreach ($result as $row) {

							$response['affiliations'][] = $row['Affiliation'];
						}
						if ($result) {
						$response['success'] = true;
						}
					}
					break;
				case "check_start_number":
					if (is_numeric($this->data['start_number'])) {
						App::import('Model', 'Runner');
						$Runner = new Runner();
						$result = $Runner->find('first', array('conditions' => "Runner.start_number = ".$this->data['start_number']));
						if ($result) {
							$response['success'] = true;
							$response['runner'] = $result['Runner'];
						}
					}
					break;
				case "add_affiliation":
					if (isset($this->data['name'])) {
					App::import('Model', 'Affiliation');
					$Affiliation = new Affiliation();
					$Affiliation->set('name', $this->data['name']);
					if ($Affiliation->save()) {
						$response['success'] = true;
						$response['affiliation']['id'] = $Affiliation->getLastInsertID();
					}
					}
					break;
				}
			}

			$this->set('response', $response);
			$this->render();
        }
    }
?>