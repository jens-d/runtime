<?php
    Configure::load('event');
	
	/*
	 * Query to update affiliation ID based on their real name:
	 * UPDATE `runners` r SET r.`affiliation_id` = (SELECT `id` FROM `affiliations` WHERE `name` LIKE r.`affiliation`) WHERE 1
	 */

    class RunnersController extends AppController {
		var $name = 'Runners';
		var $helpers = array('Html','Javascript');
		
		function index() {
				
			$runners = $this->Runner->find('all', array(
					'fields' => array('Runner.id', 'Runner.start_number', 'Runner.first_name', 'Runner.last_name', 'Runner.birth_year', 'Affiliation.name')
				)
			);
			foreach ($runners as &$runner) {
				$runner['Runner']['birth_year'] = $runner['Runner']['birth_year'] == '0000' ? '-' : $runner['Runner']['birth_year'];
			}
			$this->set('runners', $runners);
		}

		function edit($id = null) {

			$EventClass = new Eventclass();
			$buffer = $EventClass->find('all', array(
				'fields' => array('EventClass.index', 'EventClass.id', 'EventClass.name'),
				'order' => array("EventClass.index ASC"),
				'conditions' => array('EventClass.event_id', Configure::read('Event.id'))
				)
			);
			foreach ($buffer as $row) {
				$event_classes[$row['EventClass']['id']] = $row['EventClass']['index'] .' - '. $row['EventClass']['name'];
			}
			$this->set('event_classes', $event_classes);
			
			$Affiliation = new Affiliation();
			$buffer = $Affiliation->find('all', array(
					'fields' => array('Affiliation.id', 'Affiliation.name'),
					'order' => array("Affiliation.name ASC")
				)
			);
			$affiliations = array('' => 'Klubblös');
			foreach ($buffer as $row) {
				$affiliations[$row['Affiliation']['id']] = $row['Affiliation']['name'];
			}
			$this->set('affiliations', $affiliations);
			
			$this->Runner->id = $id;
			if (empty($this->data)) {
				$this->data = $this->Runner->read();
			} else {
			if ($this->Runner->save($this->data)) {
				$this->Session->setFlash('Sparade ändringarna.');
				$this->redirect(array('action' => 'edit', 'id' => $id));
			}
			}
		}

		function add() {

			$EventClass = new Eventclass();
			$buffer = $EventClass->find('all', array(
				'fields' => array('EventClass.index', 'EventClass.id', 'EventClass.name'),
				'conditions' => array('EventClass.event_id', Configure::read('Event.id'))
				)
			);
			foreach ($buffer as $row) {
				$event_classes[$row['EventClass']['id']] = $row['EventClass']['index'] .' - '. $row['EventClass']['name'];
			}
			$this->set('event_classes', $event_classes);
			if (!empty($this->data)) {

				if ($this->Runner->save($this->data)) {
					$this->Session->setFlash(sprintf('Sparade deltagaren med startnummer %d.', $this->data['Runner']['start_number']));
					$this->Session->write('Runner.event_class', $this->data['Runner']['event_class_id']);
					$this->redirect(array('action' => 'add'));
				}
			}
			$this->set('runners', $this->Runner->find('all'));
			$this->set('event_class', $this->Session->read('Runner.event_class'));
		}
		
		function search() {
		
			if (!empty($this->data)) {
			
				$Runner = new Runner();
				$where = '';
				if (isset($this->data['Runner']['start_number']) && is_numeric($this->data['Runner']['start_number'])) {
				    $where = "`Runner`.`start_number` = ". $this->data['Runner']['start_number'];
				} else {

				    if ($this->data['Runner']['first_name'] != '') {

					$where = sprintf("`Runner`.`first_name` LIKE '%%%s%%'",
						mysql_real_escape_string($this->data['Runner']['first_name'])
					);
				    }
				    if ($this->data['Runner']['last_name'] != '') {

					$where .= $where ?  " OR " : "";
					$where .= sprintf("`Runner`.`last_name` LIKE '%%%s%%'",
						mysql_real_escape_string($this->data['Runner']['last_name'])
					);
				    }
				}
				$where .= $where ?  " AND " : "";
				$where .= " `Runner`.`event_id` = ". Configure::read('Event.id');

				$search_query = "SELECT `Runner`.`id`, `Runner`.`first_name`, `Runner`.`last_name`, `Runner`.`start_number`, `Runner`.`birth_year`, `Affiliation`.`name` FROM `runners` AS `Runner` LEFT JOIN `affiliations` AS `Affiliation` ON (`Runner`.`affiliation_id` = `Affiliation`.`id`) WHERE ". $where;
				$search_result = $Runner->query($search_query);
				if ($Runner->getNumRows() == 1) {
				    $this->redirect(array('action' => 'edit', 'id' => $search_result[0]['Runner']['id']));
				}
				$this->set('search_result', $search_result);
			}
		}
    }
?>