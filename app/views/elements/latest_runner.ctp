<h2 class="first">Senaste inlagda deltagaren</h2>
<p>
<?php
App::import('Model', 'Runner');
$this->Runner = new Runner();
$runner = $this->Runner->find('first', array('order' => 'Runner.id DESC'));
$runner['Affiliation']['name'] = $runner['Affiliation']['name'] ? $runner['Affiliation']['name'] : 'klubblös';
printf("%s %s ( %s ) i klassen %s", $runner['Runner']['first_name'], $runner['Runner']['last_name'], $runner['Affiliation']['name'], $runner['EventClass']['name']);
?>
<br /><br />
<?php echo $html->link('Visa alla deltagare', array('controller' => 'runners')); ?>
</p>
