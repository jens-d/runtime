<div class="menubar">
	<ul class="menu vertical">
		<li><?php echo $html->link('Registrera ny', array('controller' => 'runners', 'action' => 'add')); ?></li>
		<li><?php echo $html->link('Lista', array('controller' => 'runners', 'action' => 'index')); ?></li>
		<li><?php echo $html->link('Sök', array('controller' => 'runners', 'action' => 'search')); ?></li>
	</ul>	
</div>
