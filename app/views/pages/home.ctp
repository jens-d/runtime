<?php
Configure::load('event');
?>
<div class="span4" id="col-left">
	<div class="menubar">
		<ul class="menu vertical">
			<li><?php echo $html->link('Visa hela startlistan', array('controller' => 'events', 'action' => 'startlist'), array('target' => '_blank')); ?></li>
		</ul>
	</div>
</div>
<div class="span8" id="col-right">
	<?php
	echo $this->element('latest_runner');
	?>
	<h2>Startlistor</h2>
	<table cellspacing="0" class="max-width table">
		<thead>
			<tr>
				<th></th>
				<th>Klass</th>
				<th>Antal anmälda deltagare</th>
			</tr>
		</thead>
		<tbody>
			<?php
			App::import('Model', 'EventClass');
			$EventClass = new EventClass();
			$classes = $EventClass->find('all', array(
					'fields' => array('EventClass.id', 'EventClass.index', 'EventClass.name'),
					'conditions' => array('EventClass.event_id' => Configure::read('Event.id'))
				)
			);
			$total_runners = 0;
			foreach ($classes as $class) {

			    $Runner = new Runner();
			    $num_runners = $Runner->find('count', array(
						'conditions' => array(
							'Runner.event_class_id' => $class['EventClass']['id'],
							'Runner.event_id' => Configure::read('Event.id')
						)
					)
			    );
				$total_runners += $num_runners;
			?>
			<tr>
				<td style="text-align: right;"><?php echo $class['EventClass']['index'];?>.</td>
				<td><?php echo $html->link($class['EventClass']['name'], array('controller' => 'events', 'action' => 'startlist', 'class' => $class['EventClass']['index']), array('target' => '_blank')); ?></td>
				<td><?php echo $num_runners;?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
	<p style="margin-top: 1em;">Totalt antal anmälda: <?php echo $total_runners;?> st.</p>
</div>