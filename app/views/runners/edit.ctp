<div class="span4" id="col-left">
    <?php echo $this->element('menu_runners'); ?>
</div>
<div class="span8" id="col-right">
    <h1 class="first">Redigera deltagare</h1>
    <?php
    $html->css("jqueryui/jquery.ui.all.css", null, null, false);
    $javascript->link("jquery-1.5.2.min.js", false);
    $javascript->link("jqueryui/jquery.ui.core.min.js", false);
    $javascript->link("jqueryui/jquery.ui.widget.min.js", false);
    $javascript->link("jqueryui/jquery.ui.position.min.js", false);
    $javascript->link("jqueryui/jquery.ui.autocomplete.min.js", false);

    echo $form->create('Runner');
    echo $form->input('event_class_id', array(
	    'label' => 'Klass',
	    'type' => 'select',
	    'options' => $event_classes
	)
    );
    echo $form->input('start_number', array('label' => 'Startnummer'));
    echo $form->input('first_name', array('label' => 'Förnamn'));
    echo $form->input('last_name', array('label' => 'Efternamn'));
    echo $form->input('birth_year', array('type' => 'text', 'label' => 'Födelseår', 'class' => 'input_year'));
    echo $form->input('affiliation_id', array(
	    'label' => 'Förening',
	    'type' => 'select',
	    'options' => $affiliations
	)
    );
    echo $form->hidden('event_id', array('value' => 1));
    echo $form->submit('Save');
    echo $form->end();
    ?>
</div>
<script type="text/javascript">
	$(function() {
		$(".autocomplete").autocomplete({
			minLength: 2,
			source: function(request, response) {
				$.ajax({
					type: "post",
					url: "/ajax/",
					dataType: "json",
					data: {
						data: {
							action: "autocomplete",
							key: "affiliation",
							short: request.term
						}
					},
					success: function(data) {
						if (data.success) {
							response($.map(data.affiliations, function(item) {
								return {
									label: item.name,
									value: item.short,
									id: item.id
								}
							}));
						}
					}
				});
			},
			select: function(event, ui) {
				if (ui.item) {
					$("#RunnerAffiliationId").val(ui.item.id);
				}
			}
		});
                $("#RunnerBirthYear").blur(function () {
                    var val = $(this).val();
                    if (val.length == 2) {
                        val = (val <= 10) ? "20" + val : "19" + val;
                        $(this).val(val);
                    }
                });
		$("#RunnerAddForm").bind("submit", function() {
		    if (!$("#RunnerAffiliationId").val()) {
			if (confirm("Det gick inte att hitta föreningen som du angivit. Vill du lägga till denna?")) {
			    
			}
			return false;
		    }
		});
		$("#RunnerStartNumber").focus();
	});
</script>