<div class="span4" id="col-left">
    <?php echo $this->element('menu_runners'); ?>
</div>
<div class="span8" id="col-right">
    <?php
    $html->css("jqueryui/jquery.ui.all.css", null, null, false);

    if (isset($search_result)) {
		
		echo '<h1 class="first">Sökresultat</h1>';
		if (count($search_result) > 0) {
	?>
    <table cellspacing="0" class="max-width">
	<thead>
	<?php
	$headers = array('Startnummer', 'Förnamn', 'Efternamn', 'Födelseår', 'Förening');
	echo $html->tableHeaders($headers);
	?>
	</thead>
	<tbody>
	<?php
	foreach ($search_result as $runner) {
	    echo '
	    <tr>
		<td>'. $html->link($runner['Runner']['start_number'], array('controller' => 'runners', 'action' => 'edit/'.$runner['Runner']['id'])).'</td>
		<td>'.$runner['Runner']['first_name'].'</td>
		<td>'.$runner['Runner']['last_name'].'</td>
		<td>'.$runner['Runner']['birth_year'].'</td>
		<td>'.$runner['Affiliation']['name'].'</td>
	    </tr>';
	}
	?>
	</tbody>
    </table>
	<?php
		} else {
			echo '<p><em>Hittade ingen matchning.</em></p>';
		}
	} else {
	
		echo '<h1 class="first">Sök deltagare</h1>';
		echo $form->create('Runner', array('action' => 'search'));
		echo $form->input('start_number', array('label' => 'Startnummer'));
		echo $form->input('first_name', array('label' => 'Förnamn'));
		echo $form->input('last_name', array('label' => 'Efternamn'));
		echo $form->hidden('event_id', array('value' => 1));
		echo $form->submit('Sök');
		echo $form->end();
	
	}
    ?>
</div>
