<div class="span4" id="col-left">
    <?php echo $this->element('menu_runners'); ?>
</div>
<div class="span8" id="col-right">
    <h1 class="first"> <?php echo count($runners);?> registrerade löpare just nu</h1>
    <table cellspacing="0" class="max-width">
		<thead>
			<?php
			$headers = array('Startnummer', 'Förnamn', 'Efternamn', 'Födelseår', 'Förening');
			echo $html->tableHeaders($headers);
			?>
		</thead>
		<tbody>
			<?php
			foreach ($runners as $runner) {
				$runner['Runner']['affiliation'] = $runner['Affiliation']['name'];
				$runner['Runner']['start_number'] =  $html->link($runner['Runner']['start_number'], array('controller' => 'runners', 'action' => 'edit/'.$runner['Runner']['id']));
				unset($runner['Runner']['id']);
				echo $html->tableCells($runner['Runner']);
			}
			?>
		</tbody>
    </table>
</div>
