<div class="span4" id="col-left">
    <?php echo $this->element('menu_runners'); ?>
</div>
<div class="span8" id="col-right">
    <h1 class="first">Ny deltagare</h1>
    <?php
    $html->css("jqueryui/jquery.ui.all.css", null, null, false);
    $javascript->link("jquery-1.5.2.min.js", false);
    $javascript->link("jqueryui/jquery.ui.core.min.js", false);
    $javascript->link("jqueryui/jquery.ui.widget.min.js", false);
    $javascript->link("jqueryui/jquery.ui.position.min.js", false);
    $javascript->link("jqueryui/jquery.ui.autocomplete.min.js", false);

    echo $form->create('Runner');
    echo $form->input('event_class_id', array(
	    'label' => 'Klass',
	    'type' => 'select',
	    'options' => $event_classes,
	    'selected' => $event_class
	)
    );
    echo $form->input('start_number', array('label' => 'Startnummer'));
    echo $form->input('first_name', array('label' => 'Förnamn'));
    echo $form->input('last_name', array('label' => 'Efternamn'));
    echo $form->input('birth_year', array('type' => 'text', 'label' => 'Födelseår', 'class' => 'input_year'));
    echo $form->input('affiliation', array('label' => 'Förening', 'class' => 'autocomplete'));
    echo $form->hidden('affiliation_id', array('value' => 0));
    echo $form->hidden('event_id', array('value' => 1));
    echo $form->submit('Spara', array('class' => 'btn'));
    echo $form->end();
    echo $html->image('ajax-loader.gif', array('id' => 'ajax-loader'));
    ?>
</div>
<script type="text/javascript">
	
	var next_number = 0;
	
	function getLastRunnerFromEventClass(event_class) {
		$.ajax({
		   type: "post",
		   url: "/ajax",
		   data: {
			   data: {
				action: "get_last_runner",
				event_class_id: event_class
			   }
		   },
		   success: function(data) {
			var text = "";
			if (data.success) {
				if (data.runner) {
					next_number = data.runner.start_number*1 + 1;
					text = 'Nästa startnummer: ' + next_number;
				}
			}
			$('#ajax-status').text(text);
		   }
		});
	}
	$(function() {
		$('.autocomplete').autocomplete({
			minLength: 2,
			source: function(request, response) {
				$.ajax({
					type: 'post',
					url: '/ajax',
					dataType: 'json',
					data: {
						data: {
							action: 'autocomplete',
							key: 'affiliation',
							term: request.term
						}
					},
					success: function(data) {
						$('#RunnerAffiliationId').val('');
						if (data.success) {
							response($.map(data.affiliations, function(item) {
								return {
									label: item.name,
									value: item.name,
									id: item.id
								}
							}));
						}
					}
				});
			},
			select: function(event, ui) {
				if (ui.item) {
					$('#RunnerAffiliationId').val(ui.item.id);
				}
			}
		});
		$('#RunnerEventClassId').change(function () {
			$('#RunnerStartNumber').val('');
			getLastRunnerFromEventClass($(this).val());
		});
		$('#RunnerBirthYear').blur(function () {
			var val = $(this).val();
			if (val.length == 2) {
				val = (val <= 10) ? "20" + val : "19" + val;
				$(this).val(val);
			}
		});
		$('#RunnerStartNumber').blur(function () {
			var val = $(this).val();
			if (val) {
				$.ajax({
				   type: "post",
				   url: "/ajax",
				   data: {
					   data: {
						action: "check_start_number",
						start_number: val
					   }
				   },
				   success: function(data) {
					var text = "";
					if (data.success) {
						if (data.runner) {
							text = 'Deltagaren finns redan registrerad!';
						}
					}
					$('#ajax-status').text(text);
				   }
				});
			}
		}).after('<span id="ajax-status" />').focus();
		
		$('#RunnerFirstName').focus(function () {
			if (!$('#RunnerStartNumber').val()) {
				$('#RunnerStartNumber').val(next_number);
			}
		});
		
		$('#RunnerAddForm').bind('submit', function() {
		    var continue_submit = true;
		    if ($('#RunnerAffiliation').val() && !$('#RunnerAffiliationId').val()) {
				var continue_submit = false;
				if (confirm('Det gick inte att hitta föreningen som du angivit. Vill du lägga till denna?')) {
					$.ajax({
					   type: 'post',
					   url: '/ajax',
					   data: {
						data: {
							action: 'add_affiliation',
							name: $('#RunnerAffiliation').val()
						}
					   },
					   success: function(data) {
						if (data.success) {
							if (data.affiliation) {
								$('#RunnerAffiliationId').val(data.affiliation.id);
								$('#RunnerAddForm').trigger('submit');
							}
						}
					   }
					});
				}
				continue_submit = false;
		    }
		    return continue_submit;
		});
		$('#RunnerStartNumber').focus();
		getLastRunnerFromEventClass($('#RunnerEventClassId').val());
	});
</script>