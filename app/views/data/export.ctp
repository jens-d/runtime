<div class="span4" id="col-left">
    <?php echo $this->element('menu_data'); ?>
</div>
<div class="span8" id="col-right">
    <h1 class="first">Exportera</h1>
    <?php
    $html->css("jqueryui/jquery.ui.all.css", null, null, false);
    $javascript->link("jquery-1.5.2.min.js", false);
    $javascript->link("jqueryui/jquery.ui.core.min.js", false);
    $javascript->link("jqueryui/jquery.ui.widget.min.js", false);
    $javascript->link("jqueryui/jquery.ui.position.min.js", false);
    $javascript->link("jqueryui/jquery.ui.autocomplete.min.js", false);

	echo $form->create('Export', array('url' => 'output'));
	echo $form->input('data_type', array(
			'label' => 'Typ av data',
			'type' => 'select',
			'options' => $data_types
		)
	);
	echo $form->input('truncate', array(
			'label' => 'Spara som lokal fil?',
			'type' => 'select',
			'options' => array(0 => 'Nej', 1 => 'Ja'),
			'selected' => 1
		)
	);
	echo $form->submit('Exportera', array('class' => 'btn'));
	echo $form->end();
    echo $html->image('ajax-loader.gif', array('id' => 'ajax-loader'));
    ?>
</div>
<script type="text/javascript">
	$('#ImportDataType').change(function(){
		if ($(this).val()) {
			this.form.submit();
		}
	});
</script>