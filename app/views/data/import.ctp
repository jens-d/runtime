<div class="span4" id="col-left">
    <?php echo $this->element('menu_data'); ?>
</div>
<div class="span8" id="col-right">
    <h1 class="first">Importera</h1>
    <?php
    $html->css("jqueryui/jquery.ui.all.css", null, null, false);
    $javascript->link("jquery-1.5.2.min.js", false);
    $javascript->link("jqueryui/jquery.ui.core.min.js", false);
    $javascript->link("jqueryui/jquery.ui.widget.min.js", false);
    $javascript->link("jqueryui/jquery.ui.position.min.js", false);
    $javascript->link("jqueryui/jquery.ui.autocomplete.min.js", false);

    echo $form->create('Import', array('url' => 'import', 'type' => 'get'));
    echo $form->input('data_type', array(
			'label' => 'Typ av data',
			'type' => 'select',
			'options' => $data_types,
			'selected' => $data_type
		)
    );
    echo $form->end();
	if (isset($data_type)) {
		echo $form->create('Import', array('url' => 'import'));
		echo $form->input('local_file', array(
				'label' => 'Välj en fil',
				'type' => 'select',
				'options' => $files
			)
		);
		if ($data_type == 2) {
			echo $form->input('truncate', array(
					'label' => 'Ersätt befintlig data?',
					'type' => 'select',
					'options' => array(0 => 'Nej', 1 => 'Ja'),
					'selected' => 1
				)
			);
		}
		echo $form->submit('Importera', array('class' => 'btn'));
		echo $form->end();
	}
    echo $html->image('ajax-loader.gif', array('id' => 'ajax-loader'));
    ?>
</div>
<script type="text/javascript">
	$('#ImportDataType').change(function(){
		if ($(this).val()) {
			this.form.submit();
		}
	});
</script>