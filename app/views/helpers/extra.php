<?php
class ExtraHelper extends AppHelper {
    var $helpers = array('Html');
	
	function _ext($file) {
		
		switch($file['filetype']) {
			case 'image/jpeg':
			case 'image/pjpeg':
			default:
				$ext = 'jpg';
		}
		return $ext;
	}
	
	function img($data, $index = 0) {
		return $this->output($this->Html->image('/files/uploads/'.$data['Bird']['id'].'.'.$this->_ext($data['Userfile'][$index]), array('alt' => $data['Bird']['name'], 'style' => 'padding: 10px;')));
    }
}
?>