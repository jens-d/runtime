<?php
Configure::load('event');
$event = ClassRegistry::init('Event')->getCurrentEvent();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
	<?php echo $html->charset(); ?>
	<title><?php echo $title_for_layout?></title>
	<!-- Include external files and scripts here (See HTML helper for more info.) --> 
	<?php echo $html->meta('icon'); ?>
	<?php echo $html->css('bootstrap.min'); ?>
	<?php echo $html->css('default'); ?>
	<?php echo $scripts_for_layout ?>
  </head>
  <body>
	<div class="row-fluid">
		<div id="header" class="span12">
			<div class="page-header"><h1 class="first"><?php echo $event['Event']['name'];?></h1></div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="navbar span12">
			<div class="navbar-inner">
			<ul class="nav">
				<li><?php echo $html->link('Start', '/');?></li>
				<li><?php echo $html->link('Deltagare', array('controller' => 'runners', 'action' => 'add'));?></li>
				<li><?php echo $html->link('Resultat', array('controller' => 'results', 'action' => 'add'));?></li>
				<li><?php echo $html->link('Import / Export', array('controller' => 'data', 'action' => 'index'));?></li>
			</ul>
			</div>
		</div>
	</div>
	<?php if ($session->valid()) { ?>
	<div class="row-fluid">
		<div class="span12">
			<?php $session->flash(); ?>
		</div>
	</div>
	<?php } ?>
	<div id="wrapper" class="row-fluid">
		<?php echo $content_for_layout; ?>
	</div>
  </body> 
</html> 