<?php
Configure::load('event');
$event = ClassRegistry::init('Event')->getCurrentEvent();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
	<?php echo $html->charset(); ?>
	<title><?php echo $title_for_layout?></title>
	<?php echo $scripts_for_layout ?>
	<style type="text/css">
		body {
			font-size: 67.5%;
		}
		body, h1, h2, h3, h4 {
			font-family: Arial, Helvetica, sans-serif;
		}

		div {
			display: block;
		}

		/* Layout */
		#header_container {
			background-color: #fff;
		}
			#header {
				width: 760px;
				margin: 0 auto;
				background-color: #fff;
			}
			#header_logo {
				padding: 15px 0;
				text-align: right;
				border-bottom: 1px solid #000;
				text-transform: uppercase;
			}

		#wrapper {
			position: relative;
			width: 760px;
			margin: 0 auto;
			background: #fff;
		}
		#footer_container {
			width: 760px;
			margin: 0 auto;
		}
			#footer {
				margin-top: 15px;
				padding: 5px 0;
				text-align: right;
				border-top: 1px solid #000;
			}

		.max-width {
			width: 100%;
		}
		table {
			table-layout: auto;
		}
		table th {
			text-align: left;
		}
		table.table-fixed {
			table-layout: fixed;
		}
		.underline {
			border-bottom: 3px solid #000;
		}
	</style>
  </head>
  <body>
	<div id="header_container">
		<div id="header">
			<div id="header_logo"><h2 class="first"><?php echo $event['Event']['name'];?></h2></div>
		</div>
	</div>
	<!-- Here's where I want my views to be displayed --> 
	<div id="wrapper">
		<?php echo $content_for_layout; ?>
	</div>
	<div id="footer_container">
		<div id="footer">Utskriftsdatum: <?php echo date("ymd H:i:s"); ?></div>
	</div>
  </body> 
</html> 