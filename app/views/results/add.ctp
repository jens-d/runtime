<div class="span4" id="col-left">
	<div class="menubar">
		<ul class="menu vertical">
			<li><?php echo $html->link('Visa resultatlistor', array('controller' => 'results', 'action' => 'index')); ?></li>
			<li><?php echo $html->link('Sök', array('controller' => 'runners', 'action' => 'search')); ?></li>
		</ul>		
	</div>
</div>
<div class="span8" id="col-right">
    <h1 class="first">Registrera nytt resultat</h1>
    <?php
    $html->css("jqueryui/jquery.ui.all.css", null, null, false);
    $javascript->link("jquery-1.5.2.min.js", false);
    $javascript->link("jqueryui/jquery.ui.core.min.js", false);
    $javascript->link("jqueryui/jquery.ui.widget.min.js", false);
    $javascript->link("jqueryui/jquery.ui.position.min.js", false);
    $javascript->link("jqueryui/jquery.ui.autocomplete.min.js", false);
    echo $form->create('Result');
    echo $form->input('start_number', array('label' => 'Startnummer'));
    echo $form->input('result', array('label' => 'Resultat (tt:mm:ss)'));
	echo $form->input('result_extra', array('label' => 'Placering'));
    echo $form->hidden('event_id', array('value' => 1));
    echo $form->hidden('runner_id');
    echo $form->hidden('event_class_id');
    echo $form->submit('Spara', array('id' => 'save_button', 'class' => 'btn'));
    echo $form->end();
    echo $html->image('ajax-loader.gif', array('id' => 'ajax-loader'));
    ?>
</div>
<script type="text/javascript">
	var result_exists;
	var s, r;
	$(function() {
		s = $("#ResultStartNumber");
		r = $("#ResultResult");
		s.blur(function () {
			var val = $(this).val();
			if (val) {
				$.ajax({
				   type: "post",
				   url: "/ajax",
				   data: {
					   data: {
						action: "check_start_number",
						start_number: val
					   }
				   },
				   success: function(data) {
					var text = '';
					var runner_id = '';
					var event_class_id = '';
					if (data.success) {
						if (data.runner.result) {
							text = 'Ett resultat (' + data.runner.result + ') finns redan!';
							result_exists = true;

						} else {

							text = '' + data.runner.first_name + ' ' + data.runner.last_name;
							result_exists = false;
						}
						runner_id = data.runner.id;
						event_class_id = data.runner.event_class_id;
						$("#ResultResultExtra").val(data.runner.result_extra);
					} else {
						text = 'Hittade ingen deltagare!';
						runner_id = '';
						event_class_id = '';
					}
					$("#ResultRunnerId").val(runner_id);
					$("#ResultEventClassId").val(event_class_id);
					$("#ajax-status").text(text);
				   }
				});
			}
		}).after('<span id="ajax-status" />').focus();
		<?php if (isset($newResultExtra)) { ?>
		
		$('#ResultResultExtra').focus().select();
		<?php } ?>
		$('#ResultAddForm').bind('submit', function() {
		    var continue_submit = true;
		    if (!s.val()) {
				s.focus();
				continue_submit = false;
		    } else if (!r.val()) {
				r.focus();
				continue_submit = false;
		    } else if (!$("#ResultRunnerId").val()) {
				alert('Det finns ingen registrerad deltagare med det angivna startnumret. Kontrollera numret och försök igen.');
				continue_submit = false;
		    } else if (result_exists) {
				continue_submit = confirm('Du kommer att ersätta det tidigare resultatet. Är du säker?');
		    }
		    return continue_submit;
		});
		$('#ajax-loader').ajaxStart(function() {
		    $(this).show();
		}).ajaxStop(function() {
		    $(this).hide();
		});
	});
</script>