    <h1 class="underline">Resultatlista</h1>
    <table cellspacing="0" class="max-width">
		<tbody>
		<?php
		$place = 1;
		$class = "";
		foreach ($runners as $runner) {
			if ($class != $runner['EventClass']['name']) {
				$class = $runner['EventClass']['name'];
				$place = 1;
				echo '
			<tr>
				<td colspan="7"><h3>'.$runner['EventClass']['index'].' - '.$class.'</h3></td>
			</tr>
			<tr>
				<th style="text-align: right; padding-right: 10px;">Placering</th>
				<th>Startnr</th>
				<th>Förnamn</th>
				<th>Efternamn</th>
				<th>Födelseår</th>
				<th>Förening</th>
				<th style="text-align: right; padding-right: 10px;">Resultat</th>
			</tr>';
			}
			echo '
			<tr>
				<td style="text-align: right; padding-right: 10px;">'.$place.'</td>
				<td>'.$runner['Runner']['start_number'].'</td>
				<td>'.$runner['Runner']['first_name'].'</td>
				<td>'.$runner['Runner']['last_name'].'</td>
				<td>'.$runner['Runner']['birth_year'].'</td>
				<td>'.$runner['Affiliation']['name'].'</td>
				<td style="text-align: right; padding-right: 10px;">'.$runner['Runner']['result'].'</td>
			</tr>';
			$place++;
		}
		?>
		</tbody>
    </table>
