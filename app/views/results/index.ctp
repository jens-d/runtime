<div class="span4" id="col-left">
	<div class="menubar">
		<ul class="menu vertical">
			<li><?php echo $html->link('Registrera nytt resultat', array('controller' => 'results', 'action' => 'add')); ?></li>
			<li><?php echo $html->link('Sök', array('controller' => 'runners', 'action' => 'search')); ?></li>
		</ul>
	</div>
</div>
<div class="span8" id="col-right">
    <h1 class="first">Välj vilken klass du vill skriva ut</h1>
	<table cellspacing="0" class="max-width medium-font">
		<thead>
			<tr>
				<th></th>
				<th>Klass</th>
				<th>Antal registrerade resultat</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($classes as $class) {
			?>
			<tr>
				<td style="text-align: right;"><?php echo $class['EventClass']['index'];?>.</td>
				<td><?php echo $html->link($class['EventClass']['name'], array('controller' => 'results', 'action' => 'resultlist', 'media' => 'print', 'class' => $class['EventClass']['id']), array('target' => '_blank')); ?></td>
				<td><?php echo $class['EventClass']['total_results'];?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>