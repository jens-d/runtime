<h1>Add blog post</h1>
<?php 
$html->css("http://yui.yahooapis.com/2.8.0r4/build/assets/skins/sam/skin.css", null, null, false);
$javascript->link("http://yui.yahooapis.com/2.8.0r4/build/yahoo-dom-event/yahoo-dom-event.js", false);
$javascript->link("http://yui.yahooapis.com/2.8.0r4/build/element/element-min.js", false);
$javascript->link("http://yui.yahooapis.com/2.8.0r4/build/container/container_core-min.js", false);
$javascript->link("http://yui.yahooapis.com/2.8.0r4/build/menu/menu-min.js", false);
$javascript->link("http://yui.yahooapis.com/2.8.0r4/build/button/button-min.js", false);
$javascript->link("http://yui.yahooapis.com/2.8.0r4/build/editor/editor-min.js", false);
echo $form->create('Post');
echo $form->input('title');
echo $form->input('body', array('rows' => '3'));
echo $form->submit('Save Post', array('id' => 'saveButton'));
echo $form->end();
?>
<script type="text/javascript">
    var myEditor = new YAHOO.widget.Editor('PostBody', {
        height: '300px',
        width: '522px',
        dompath: true, //Turns on the bar at the bottom
        animate: true, //Animates the opening, closing and moving of Editor windows
        toolbar: {
            buttons: [
                { group: 'textstyle', label: 'Font Style',
                    buttons: [
                        { type: 'push', label: 'Bold', value: 'bold' },
                        { type: 'push', label: 'Italic', value: 'italic' },
                        { type: 'push', label: 'Underline', value: 'underline' },
                        { type: 'separator' },
                        { type: 'select', label: 'Arial', value: 'fontname', disabled: true,
                            menu: [
                                { text: 'Arial', checked: true },
                                { text: 'Trebuchet MS' },
                                { text: 'Verdana' }
                            ]
                        },
                        { type: 'spin', label: '13', value: 'fontsize', range: [ 9, 75 ], disabled: true }
                    ]
                }
            ]
        },
        editorClick: function() {alert("OK");}
    });
    myEditor.render();

    YAHOO.util.Event.on('saveButton', 'click', function(e) {
        //Put the HTML back into the text area
        myEditor.saveHTML();

        //The var html will now have the contents of the textarea
        var html = myEditor.get('element').value;
    });
</script>
