    <h1 class="underline">Startlista</h1>
	<h2><strong>Antal anmälda: <?php echo count($runners);?> st.</strong></h2>
    <table cellspacing="0" class="max-width">
		<thead>
		<?php
		$headers = array('Startnr', 'Förnamn', 'Efternamn', 'Födelseår', 'Förening');
		?>
		</thead>
		<tbody>
		<?php
		$class = "";
		foreach ($runners as $runner) {
			if ($class != $runner['EventClass']['name']) {
				$class = $runner['EventClass']['name'];
				echo '
			<tr>
				<td colspan="7"><h3>'. $runner['EventClass']['index']. ' - '. $class. '</h3></td>
			</tr>';
				echo $html->tableHeaders($headers);
			}
			echo '
			<tr>
				<td>'.$runner['Runner']['start_number'].'</td>
				<td>'.$runner['Runner']['first_name'].'</td>
				<td>'.$runner['Runner']['last_name'].'</td>
				<td>'.$runner['Runner']['birth_year'].'</td>
				<td>'.$runner['Affiliation']['name'].'</td>
			</tr>';
			$total++;
		}
		?>
		</tbody>
    </table>
