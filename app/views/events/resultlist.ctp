<div class="column" id="col-left">
    <ul class="menu vertical">
	<li><?php echo $html->link('Visa resultatlista', array('controller' => 'events', 'action' => 'resultlist')); ?></li>
	<li><?php echo $html->link('Sök', array('controller' => 'runners', 'action' => 'search')); ?></li>
    </ul>
</div>
<div class="column" id="col-right">
    <h1 class="first">Resultatlista</h1>
    <table cellspacing="0" class="max-width">
	<thead>
	<?php
	$headers = array('Placering', 'Resultat', 'Startnr', 'Förnamn', 'Efternamn', 'Födelseår', 'Förening');
	echo $html->tableHeaders($headers);
	?>
	</thead>
	<tbody>
	<?php
	$place = 1;
	foreach ($runners as $runner) {
	    echo '
	    <tr>
		<td>'.$place.'</td>
		<td>'.$runner['Runner']['result'].'</td>
		<td>'.$runner['Runner']['start_number'].'</td>
		<td>'.$runner['Runner']['first_name'].'</td>
		<td>'.$runner['Runner']['last_name'].'</td>
		<td>'.$runner['Runner']['birth_year'].'</td>
		<td>'.$runner['Affiliation']['name'].'</td>
	    </tr>';
	    $place++;
	}
	?>
	</tbody>
    </table>
</div>
